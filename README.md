# HDPS

- Activate Virtualenv
```bash
sudo apt install python3-venv
python3 -m venv venv
source venv/bin/activate
```
- `pip install -r requirements.txt` Run this command to install django and python packages.
- Then run this three command to initiate database.
```bash
python manage.py makemigrations
python manage.py migrate
```
- `python manage.py runserver` After all of this finally run this command to run project
