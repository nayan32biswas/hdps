FROM python:3.8

WORKDIR /project/

COPY ./requirements.txt /project/requirements.txt
RUN pip install -r /project/requirements.txt

ADD ./ /project/
