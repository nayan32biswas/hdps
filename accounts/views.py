from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout,
)
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
User = get_user_model()

from .models import (
    Disease,
    KNN,
    DTree,
    RForest,
    Adaboost,
    LRegression,
    SVM,
    Nbayes,
    XGBoost,
)

from .forms import UserLoginForm, UserRegisterForm, DiseaseForm
from .utls.prediction import (
    f_KNN,
    f_DTree,
    f_RForest,
    f_Adaboost,
    f_LRegression,
    f_SVM,
    f_Nbayes,
    f_XGBoost,
)
# Create your views here.


def home_page(request):
    next = request.GET.get('next')
    login_title = "Login"
    login_form = UserLoginForm(request.POST or None)
    if login_form.is_valid():
        username = login_form.cleaned_data.get("username")
        password = login_form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        if next:
            return redirect(next)
        return redirect("/users/disease/")
    # Home page login end
    next = request.GET.get('next')
    register_title = "Register"
    register_form = UserRegisterForm(request.POST or None)
    if register_form.is_valid():
        user = register_form.save(commit=False)
        password = register_form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)
        if next:
            return redirect(next)
        return redirect("/users/disease/")
    # Home page registration end
    context = {
        "login_form": login_form,
        "login_title": login_title,
        "register_form": register_form,
        "register_title": register_title
    }
    return render(request, "home.html", context)


def user_profile(request):
    return render(request, "profile.html", {})


def calculate_disease(request):
    title = "Check Disease"
    form = DiseaseForm(request.POST or None)
    if request.user.is_authenticated and form.is_valid():

        age = form.cleaned_data.get("age")
        sex = form.cleaned_data.get("sex")
        cp = form.cleaned_data.get("cp")
        trestbps = form.cleaned_data.get("trestbps")
        chol = form.cleaned_data.get("chol")
        fbs = form.cleaned_data.get("fbs")
        restecg = form.cleaned_data.get("restecg")
        thalach = form.cleaned_data.get("thalach")
        exang = form.cleaned_data.get("exang")
        oldpeak = form.cleaned_data.get("oldpeak")
        slope = form.cleaned_data.get("slope")
        ca = form.cleaned_data.get("ca")
        thal = form.cleaned_data.get("thal")
        input_array = [age, sex, cp, trestbps, chol, fbs, restecg, thalach, exang, oldpeak, slope, ca, thal]
        
        disease_instance = Disease.objects.create(
            user=request.user,
            age=age,
            sex=sex,
            cp=cp,
            trestbps=trestbps,
            chol=chol,
            fbs=fbs,
            restecg=restecg,
            thalach=thalach,
            exang=exang,
            oldpeak=oldpeak,
            slope=slope,
            ca=ca,
            thal=thal
        )
        predict_final(disease_instance, input_array)
        return redirect("/users/disease/show")
    context = {"form": form, "title": title}
    return render(request, "disease_form.html", context)


def predict_final(disease_instance, input_array):
    result, confusion_matrix, accuracy = f_KNN(input_array)
    knn_instance = KNN.objects.create(
        result=result,
        accuracy=accuracy,
        disease=disease_instance,
        con_00=confusion_matrix[0][0],
        con_01=confusion_matrix[0][1],
        con_10=confusion_matrix[1][0],
        con_11=confusion_matrix[1][1],
    )
    result, confusion_matrix, accuracy = f_DTree(input_array)
    dtree_instance = DTree.objects.create(
        result=result,
        accuracy=accuracy,
        disease=disease_instance,
        con_00=confusion_matrix[0][0],
        con_01=confusion_matrix[0][1],
        con_10=confusion_matrix[1][0],
        con_11=confusion_matrix[1][1],
    )
    result, confusion_matrix, accuracy = f_RForest(input_array)
    rforest_instance = RForest.objects.create(
        result=result,
        accuracy=accuracy,
        disease=disease_instance,
        con_00=confusion_matrix[0][0],
        con_01=confusion_matrix[0][1],
        con_10=confusion_matrix[1][0],
        con_11=confusion_matrix[1][1],
    )
    result, confusion_matrix, accuracy = f_Adaboost(input_array)
    adaboost_instance = Adaboost.objects.create(
        result=result,
        accuracy=accuracy,
        disease=disease_instance,
        con_00=confusion_matrix[0][0],
        con_01=confusion_matrix[0][1],
        con_10=confusion_matrix[1][0],
        con_11=confusion_matrix[1][1],
    )
    result, confusion_matrix, accuracy = f_LRegression(input_array)
    lregression_instance = LRegression.objects.create(
        result=result,
        accuracy=accuracy,
        disease=disease_instance,
        con_00=confusion_matrix[0][0],
        con_01=confusion_matrix[0][1],
        con_10=confusion_matrix[1][0],
        con_11=confusion_matrix[1][1],
    )
    result, confusion_matrix, accuracy = f_SVM(input_array)
    svm_instance = SVM.objects.create(
        result=result,
        accuracy=accuracy,
        disease=disease_instance,
        con_00=confusion_matrix[0][0],
        con_01=confusion_matrix[0][1],
        con_10=confusion_matrix[1][0],
        con_11=confusion_matrix[1][1],
    )
    result, confusion_matrix, accuracy = f_Nbayes(input_array)
    nbayes_instance = Nbayes.objects.create(
        result=result,
        accuracy=accuracy,
        disease=disease_instance,
        con_00=confusion_matrix[0][0],
        con_01=confusion_matrix[0][1],
        con_10=confusion_matrix[1][0],
        con_11=confusion_matrix[1][1],
    )
    result, confusion_matrix, accuracy = f_XGBoost(input_array)
    xgboost_instance = XGBoost.objects.create(
        result=result,
        accuracy=accuracy,
        disease=disease_instance,
        con_00=confusion_matrix[0][0],
        con_01=confusion_matrix[0][1],
        con_10=confusion_matrix[1][0],
        con_11=confusion_matrix[1][1],
    )


def view_disease(request):
    context = {"no_disease": "You never check any disease"}
    if request.user.is_authenticated:
        queryset_list = Disease.objects.filter(user=request.user).order_by("-id")
        if queryset_list.count():
            data = []
            for each in queryset_list:
                print(each.id)
                current = {}
                current["disease"] = each
                current["knn"] = KNN.objects.filter(disease=each.id).first()
                current["dtree"] = DTree.objects.filter(disease=each.id).first()
                current["rforest"] = RForest.objects.filter(disease=each.id).first()
                current["adaboost"] = Adaboost.objects.filter(disease=each.id).first()
                current["lregression"] = LRegression.objects.filter(disease=each.id).first()
                current["svm"] = SVM.objects.filter(disease=each.id).first()
                current["nbayes"] = Nbayes.objects.filter(disease=each.id).first()
                current["xgboost"] = XGBoost.objects.filter(disease=each.id).first()
                data.append(current)
            context["data_list"] = data
            return render(request, "disease_view.html", context)
        else:
            return render(request, "disease_view.html", context)
    return render(request, "disease_view.html", context)

def view_dashboard(request):
    context = {"message": "You never check any disease"}
    queryset_list = User.objects.filter()
    male, female = 0, 0
    print("start")
    for user in queryset_list:
        m = KNN.objects.filter(disease__user=user , disease__sex=True).count()
        f = KNN.objects.filter(disease__user=user , disease__sex=False).count()
        print(user, m, f)
        male+=m
        female+=f
    # context["data_list"] = data
    context["male"] = male
    context["female"] = female
    print(context)
    return render(request, "show_statistics.html", context)


def login_view(request):
    next = request.GET.get('next')
    title = "Login"
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        if next:
            return redirect(next)
        return redirect("/users/disease/")
    context = {"form": form, "title": title}
    return render(request, "login.html", context)


def register_view(request):
    next = request.GET.get('next')
    title = "Register"
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)
        if next:
            return redirect(next)
        return redirect("/users/disease/")

    context = {
        "form": form,
        "title": title
    }
    return render(request, "registration.html", context)


def logout_view(request):
    logout(request)
    return redirect("/")
