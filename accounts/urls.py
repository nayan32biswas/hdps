from django.urls import path
from .views import (
    register_view,
    login_view,
    logout_view,
    user_profile,
    calculate_disease,
    view_disease,
    view_dashboard
)
urlpatterns = [
    path("profile/", user_profile, name="profile"),
    path("register/", register_view, name="register"),
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("disease/", calculate_disease, name="disease"),
    path("disease/show", view_disease, name="disease_show"),
    path("statistics/show", view_dashboard, name="statistics"),
]
