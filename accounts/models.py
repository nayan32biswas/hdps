from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()
# Create your models here.


class Disease(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    age = models.IntegerField()
    sex = models.BooleanField()
    cp = models.IntegerField()
    trestbps = models.IntegerField()
    chol = models.IntegerField()
    fbs = models.IntegerField()
    restecg = models.IntegerField()
    thalach = models.IntegerField()
    exang = models.IntegerField()
    oldpeak = models.FloatField()
    slope = models.IntegerField()
    ca = models.IntegerField()
    thal = models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)


class KNN(models.Model):
    disease = models.ForeignKey(Disease, on_delete=models.CASCADE)
    result = models.BooleanField()
    accuracy = models.FloatField()
    con_00 = models.IntegerField()
    con_01 = models.IntegerField()
    con_10 = models.IntegerField()
    con_11 = models.IntegerField()


class DTree(models.Model):
    disease = models.ForeignKey(Disease, on_delete=models.CASCADE)
    result = models.BooleanField()
    accuracy = models.FloatField()
    con_00 = models.IntegerField()
    con_01 = models.IntegerField()
    con_10 = models.IntegerField()
    con_11 = models.IntegerField()


class RForest(models.Model):
    disease = models.ForeignKey(Disease, on_delete=models.CASCADE)
    result = models.BooleanField()
    accuracy = models.FloatField()
    con_00 = models.IntegerField()
    con_01 = models.IntegerField()
    con_10 = models.IntegerField()
    con_11 = models.IntegerField()


class Adaboost(models.Model):
    disease = models.ForeignKey(Disease, on_delete=models.CASCADE)
    result = models.BooleanField()
    accuracy = models.FloatField()
    con_00 = models.IntegerField()
    con_01 = models.IntegerField()
    con_10 = models.IntegerField()
    con_11 = models.IntegerField()


class LRegression(models.Model):
    disease = models.ForeignKey(Disease, on_delete=models.CASCADE)
    result = models.BooleanField()
    accuracy = models.FloatField()
    con_00 = models.IntegerField()
    con_01 = models.IntegerField()
    con_10 = models.IntegerField()
    con_11 = models.IntegerField()


class SVM(models.Model):
    disease = models.ForeignKey(Disease, on_delete=models.CASCADE)
    result = models.BooleanField()
    accuracy = models.FloatField()
    con_00 = models.IntegerField()
    con_01 = models.IntegerField()
    con_10 = models.IntegerField()
    con_11 = models.IntegerField()


class Nbayes(models.Model):
    disease = models.ForeignKey(Disease, on_delete=models.CASCADE)
    result = models.BooleanField()
    accuracy = models.FloatField()
    con_00 = models.IntegerField()
    con_01 = models.IntegerField()
    con_10 = models.IntegerField()
    con_11 = models.IntegerField()


class XGBoost(models.Model):
    disease = models.ForeignKey(Disease, on_delete=models.CASCADE)
    result = models.BooleanField()
    accuracy = models.FloatField()
    con_00 = models.IntegerField()
    con_01 = models.IntegerField()
    con_10 = models.IntegerField()
    con_11 = models.IntegerField()
