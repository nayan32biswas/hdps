from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from warnings import simplefilter

# import numpy as np

# import matplotlib.pyplot as plt
import pandas as pd

# import seaborn as sns

from sklearn import svm
from sklearn.metrics import confusion_matrix

from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import RandomForestClassifier
import warnings

warnings.filterwarnings("ignore")
simplefilter(action="ignore", category=FutureWarning)

data = pd.read_csv("heart.csv")

X = data.iloc[:, :-1].values
y = data.iloc[:, 13].values

minmax = preprocessing.MinMaxScaler(feature_range=(0, 1))
x = minmax.fit(X).transform(X)

X_train, X_test, y_train, y_test = train_test_split(
    x, y, test_size=0.25, random_state=0
)

max_x = {}
min_x = {}
features = [
    "age",
    "sex",
    "cp",
    "trestbps",
    "chol",
    "fbs",
    "restecg",
    "thalach",
    "exang",
    "oldpeak",
    "slope",
    "ca",
    "thal",
]
for i in range(len(features)):
    max_x[features[i]] = data.loc[data[features[i]].idxmax()][features[i]]
    min_x[features[i]] = data.loc[data[features[i]].idxmin()][features[i]]


def f_KNN(input_array):
    features = [
        "age",
        "sex",
        "cp",
        "trestbps",
        "chol",
        "fbs",
        "restecg",
        "thalach",
        "exang",
        "oldpeak",
        "slope",
        "ca",
        "thal",
    ]

    def f_scaled_input(raw_input):
        return [
            (
                (raw_input[i] - min_x[features[i]])
                / (max_x[features[i]] - min_x[features[i]])
            )
            * ((1 - 0) + 0)
            for i in range(len(raw_input))
        ]

    raw_input = input_array
    final_input = f_scaled_input(raw_input)

    classifier = KNeighborsClassifier(n_neighbors=10, metric="minkowski", p=2)
    classifier = classifier.fit(X_train, y_train)

    y_pred = classifier.predict(X_test)

    from sklearn import metrics

    accuracy = metrics.accuracy_score(y_test, y_pred)
    # check accuracy
    print("Accuracy: {:.2f}".format(accuracy))

    # print('Accuracy: {:.2f}'.format(accuracy))
    result = classifier.predict([final_input])[0]  # output prediction of KNN
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import confusion_matrix, classification_report

    confusion_matrix = confusion_matrix(y_test, y_pred)
    # accuracy = classification_report(y_pred, y_test)
    return result, confusion_matrix, accuracy


def f_DTree(input_array):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0
    )
    # Create Decision Tree classifer object
    clf = DecisionTreeClassifier()
    # Train Decision Tree Classifer
    clf = clf.fit(X_train, y_train)
    # Predict the response for test dataset
    y_pred = clf.predict(X_test)
    result = clf.predict([input_array])
    # Model Accuracy, how often is the classifier correct?
    accuracy = accuracy_score(y_test, y_pred)
    # print('Accuracy: {:.2f}'.format(accuracy))
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import confusion_matrix, classification_report

    confusion_matrix = confusion_matrix(y_test, y_pred)
    # print("Confusion matrix:\n%s" % confusion_matrix)
    # print(classification_report(y_pred, y_test))

    return result, confusion_matrix, accuracy


def f_RForest(input_array):
    data.columns = [
        "age",
        "sex",
        "cp",
        "trestbps",
        "chol",
        "fbs",
        "restecg",
        "thalach",
        "exang",
        "oldpeak",
        "slope",
        "ca",
        "thal",
        "target",
    ]
    from sklearn.model_selection import train_test_split

    clfr = RandomForestClassifier(n_estimators=100)

    # Train the model using the training sets y_pred=clf.predict(X_test)
    clfr.fit(X_train, y_train)

    # prediction on test set
    y_pred = clfr.predict(X_test)

    result = clfr.predict([input_array])

    # Import scikit-learn metrics module for accuracy calculation
    from sklearn import metrics

    # Model Accuracy, how often is the classifier correct?
    accuracy_score = metrics.accuracy_score(y_test, y_pred)

    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import confusion_matrix, classification_report

    confusion_matrix = confusion_matrix(y_test, y_pred)
    # print("Confusion matrix of random forest:\n%s" % confusion_matrix)
    # print(classification_report(y_pred, y_test))

    return result, confusion_matrix, accuracy_score


def f_Adaboost(input_array):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0
    )  # 70% training and 30% test
    from sklearn.ensemble import AdaBoostClassifier

    # Create adaboost classifer object
    abc = AdaBoostClassifier(n_estimators=20, learning_rate=1)
    # Train Adaboost Classifer
    model = abc.fit(X_train, y_train)
    # print('prediction result of ADAboost')
    result = model.predict([input_array])
    from sklearn.metrics import confusion_matrix
    from sklearn import metrics

    y_pred = model.predict(X_test)
    cm = confusion_matrix(y_test, y_pred)
    # print("Accuracy of adaboost:", metrics.accuracy_score(y_test, y_pred))
    # print()
    accuracy_score = (cm[0][0] + cm[1][1]) / len(y_train)
    # print('Accuracy for test set for Random Forest = {}'.format((cm[0][0] + cm[1][1])/len(y_test)))

    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import confusion_matrix, classification_report

    confusion_matrix = confusion_matrix(y_test, y_pred)
    # print("Confusion matrix ada boost:\n%s" % confusion_matrix)
    # print(classification_report(y_pred, y_test))

    return result, confusion_matrix, accuracy_score


def f_LRegression(input_array):
    from sklearn import metrics
    from sklearn.linear_model import LogisticRegression

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.30, random_state=0
    )
    lr = LogisticRegression()
    lr.fit(X_train, y_train)

    y_pred = lr.predict(X_test)
    result = lr.predict([[35, 1, 0, 126, 282, 0, 0, 156, 1, 0, 2, 0, 2]])

    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import confusion_matrix, classification_report

    confusion_matrix = confusion_matrix(y_test, y_pred)
    # print("Confusion matrix logistic regresion:\n%s" % confusion_matrix)
    # print(classification_report(y_pred, y_test))

    accuracy_score = metrics.accuracy_score(y_test, y_pred)
    # print("Precision:", metrics.precision_score(y_test, y_pred))
    # print("Recall:", metrics.recall_score(y_test, y_pred))

    return result, confusion_matrix, accuracy_score


def f_SVM(input_array):
    # Import train_test_split function
    from sklearn.model_selection import train_test_split

    # Split dataset into training set and test set
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=1
    )
    from sklearn import svm

    # Create a svm Classifier
    clfs = svm.SVC(kernel="linear")  # Linear Kernel
    # Train the model using the training sets
    clfs.fit(X_train, y_train)
    # Predict the response for test dataset
    y_pred = clfs.predict(X_test)
    result = clfs.predict([[35, 1, 0, 126, 282, 0, 0, 156, 1, 0, 2, 0, 2]])
    # Import scikit-learn metrics module for accuracy calculation
    from sklearn import metrics

    # Model Accuracy: how often is the classifier correct?
    accuracy = metrics.accuracy_score(y_test, y_pred)
    print(
        "Accuracy:",
    )
    # # Model Precision: what percentage of positive tuples are labeled as such?
    # print("Precision:", metrics.precision_score(y_test, y_pred))
    # # Model Recall: what percentage of positive tuples are labelled as such?
    # print("Recall:", metrics.recall_score(y_test, y_pred))

    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import confusion_matrix, classification_report

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print("Confusion matrix SVM:\n%s" % confusion_matrix)
    print(classification_report(y_pred, y_test))

    return result, confusion_matrix, accuracy


def f_Nbayes(input_array):
    from sklearn.model_selection import train_test_split

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=0
    )

    from sklearn.naive_bayes import GaussianNB

    classifier = GaussianNB()
    classifier.fit(X_train, y_train)

    # Predicting the Test set results
    y_pred = classifier.predict(X_test)

    from sklearn.metrics import confusion_matrix

    cm_test = confusion_matrix(y_pred, y_test)

    y_pred_train = classifier.predict(X_train)
    confusion_matrix = confusion_matrix(y_pred_train, y_train)

    # print()
    accuracy_score = (confusion_matrix[0][0] + confusion_matrix[1][1]) / len(y_train)
    # print('Accuracy for test set for Naive Bayes = {}'.format((cm_test[0][0] + cm_test[1][1])/len(y_test)))
    result = classifier.predict([input_array])
    return result, confusion_matrix, accuracy_score


def f_XGBoost(input_array):
    from xgboost import XGBClassifier

    xg = XGBClassifier()
    xg.fit(X_train, y_train)
    y_pred = xg.predict(X_test)

    from sklearn.metrics import confusion_matrix

    cm_test = confusion_matrix(y_pred, y_test)

    y_pred_train = xg.predict(X_train)

    for i in range(0, len(y_pred_train)):
        if y_pred_train[i] >= 0.5:  # setting threshold to .5
            y_pred_train[i] = 1
        else:
            y_pred_train[i] = 0

    confusion_matrix = confusion_matrix(y_pred_train, y_train)
    # print()
    # print('Accuracy for training set for XGBoost = {}'.format((confusion_matrix[0][0] + confusion_matrix[1][1])/len(y_train)))
    # print('Accuracy for test set for XGBoost = {}'.format((cm_test[0][0] + cm_test[1][1])/len(y_test)))
    accuracy_score = (confusion_matrix[0][0] + confusion_matrix[1][1]) / len(y_train)
    result = xg.predict([input_array])

    # print("Confusion matrix XGboost:\n%s" % confusion_matrix)
    return result, confusion_matrix, accuracy_score
